# SGE - Sistema para Gerenciamento de Estacionamento - API


## 🔧 Instalação

<h4>Você precisa instalar o Node.js antes de tudo, para depois clonar o projeto via HTTPS ou SSH.</h4>
<h4>Instale o Docker em seu dispositivo.</h4>
<h4>Rode esse comando:</h4>

<strong>HTTPS</strong>
<p><code>git clone https://gitlab.com/algoritmos-iii-2021/projeto-sge/sge_api.git</code></p>
<p>Ou</p>
<strong>SSH</strong>
<p><code>git clone git@gitlab.com:algoritmos-iii-2021/projeto-sge/sge_api.git</code></p>

<h4>Agora falta instalar as dependências. Rode esse comando:</h4>

<p><code>npm install</code></p>

<h4>Back-end</h4>
  <ul>
       <li>
        Navege até a pasta raiz do projeto
        </li>
        <li>
           Agora rode esse comando:
            <code>docker-compose up</code>
        </li>
          
   </ul>

## 🛠️ Construído com
<ul>
<li>Typescript</li>
<li>Express</li>
<li>Docker</li>
<li>Postgres</li>
</ul>


## ✒️ Autores
<ul>
<li><a href="https://gitlab.com/cleoreggio">Cléo Reggio</li>
<li><a href="https://gitlab.com/pablovmartins">Pablo Martins</li>
<li><a href="https://gitlab.com/wagnerandersson">Wagner anderson</li>
<li><a href="http://gitlab.com/W-araujo">Wesley Araujo</li>
</ul>
