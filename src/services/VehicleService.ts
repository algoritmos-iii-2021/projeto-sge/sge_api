import { VehicleRepository } from '../repositories/VehicleRepository'
import { ClientRepository } from '../repositories/ClientRepository'
import { getCustomRepository, Like } from "typeorm"

import AppError from '../errors/Errors'

interface Request {
    model: string
    color: string
    license: string
    category: string
    typeVehicle: string
    clientId: number
}

export default class VehicleService {

    async create({ model, color, license, category, typeVehicle, clientId }: Request) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        if (!license || !model || !color) {
            throw new AppError('Faltou dados do veículo!')
        }

        const vehicleAlreadyExists = await vehicleRepository.findOne({
            license
        })

        if (vehicleAlreadyExists) {
            throw new AppError('Veículo já existente!')
        }

        const vehicle = vehicleRepository.create({
            model,
            color,
            license,
            category,
            typeVehicle,
            clientId
        })

        await vehicleRepository.save(vehicle)
        return vehicle
    }

    async list() {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const all = await vehicleRepository.find()

        return all
    }

    async getBylicense(license: string) {

        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne({ license })

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        return vehicle
    }

    async getById(id: number) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        return vehicle
    }

    async delete(id: number) {
        const vehicleRepository = getCustomRepository(VehicleRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const deleted = await vehicleRepository.delete(id)

        return deleted
    }

    async Update(id: number, { model, color, license, category, typeVehicle, clientId }: Request) {
        const vehicleRepository = getCustomRepository(VehicleRepository)
        const clientRepository = getCustomRepository(ClientRepository)

        const vehicle = await vehicleRepository.findOne(id)

        if (!vehicle) {
            throw new AppError('Veículo não encontrado!')
        }

        const licenseAreadyExists = await vehicleRepository.findOne({ license })

        const clientAlreadyExists = await clientRepository.findOne({ id: clientId })

        if (!clientAlreadyExists) {
            throw new AppError('Veículo não existente!')
        }

        if (license === vehicle.license || !licenseAreadyExists) {
            await vehicleRepository.update(id, {
                model, color, license, category, typeVehicle, clientId
            })

            const vehicleUpdate = await vehicleRepository.findOne(id)

            return vehicleUpdate
        }
        throw new AppError("Veículo já cadastrado");
    }

}


