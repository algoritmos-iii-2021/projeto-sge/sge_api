import { EmployeeRepository } from '../repositories/EmployeeRepository'
import UserConfigService from '../services/UserConfigService'
import { getCustomRepository, Like } from "typeorm"

import AppError from '../errors/Errors'

interface Request {
    name: string
    password: string
    email: string
    birth: Date
    cpf: string
    address: string
    admin: boolean
    phone: string
}

export default class EmployeeService {

    async create({ name, password, email, birth, cpf, address, admin, phone }: Request) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const userConfigService = new UserConfigService()

        const employeeAlreadyExists = await employeeRepository.findOne({
            cpf
        })

        if (employeeAlreadyExists) {
            throw new AppError('Funcionário já existente')
        }

        const employee = employeeRepository.create({
            name: name.toUpperCase(),
            password,
            email,
            birth,
            cpf,
            address,
            admin,
            phone,
        })
        
        await employeeRepository.save(employee)
        
        if(admin) {
            userConfigService.create({ 
                employeeConfigId: employee.id, 
                register: true, 
                report: true, 
                vehicleUpdate: true 
            })
        }
        else {
            userConfigService.create({ 
                employeeConfigId: employee.id, 
                register: false, 
                report: false, 
                vehicleUpdate: false 
            })
        }
        
        return employee
    }

    async list() {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const all = await employeeRepository.find()

        return all
    }

    async getById(id: number) {
        
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }

    async getByName(name: string) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.find({ name: Like(`%${name.toUpperCase()}%`) })
        
        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }
    
    async getByCpf(cpf: string) {

        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.find({ cpf: Like(`%${cpf}%`) })

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        return employee
    }

    async delete(id: number) {
        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        const deleted = await employeeRepository.delete(id)

        return deleted
    }

    async Update(id: number, { name, password, phone, email, birth, cpf, address, admin }: Request) {

        const employeeRepository = getCustomRepository(EmployeeRepository)

        const employee = await employeeRepository.findOne(id)

        if (!employee) {
            throw new AppError('Funcionário não encontrado!')
        }

        await employeeRepository.update(id, {
            name, password, phone, email, birth, cpf, address, admin
        })

        const employeeUpdate = await employeeRepository.findOne(id)

        return employeeUpdate
    }

}

