import { CashierRepository } from '../repositories/CashierRepository'
import { getCustomRepository } from "typeorm"

import AppError from '../errors/Errors'

export interface Request {
    startingAmount: number
    endingAmount: number
    startDate: Date
    endDate: Date
    countAmount: number
}

export default class CashierService {

    async create({ startingAmount, endingAmount, startDate, endDate, countAmount }: Request) {
        
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = cashierRepository.create({
            startingAmount,
            endingAmount,
            startDate,
            endDate,
            countAmount
        })

        await cashierRepository.save(cashier)
        return cashier
    }

    async list() {
        const cashierRepository = getCustomRepository(CashierRepository)

        const all = await cashierRepository.find()

        return all
    }

    async getById(id: number) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        return cashier
    }

    async delete(id: number) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) {
            throw new AppError('Caixa não encontrado!')
        }

        const deleted = await cashierRepository.delete(id)

        return deleted
    }

    async Update(id: number, { startingAmount, endingAmount, startDate, endDate, countAmount }: Request) {
        const cashierRepository = getCustomRepository(CashierRepository)

        const cashier = await cashierRepository.findOne(id)

        if (!cashier) {
            throw new AppError('Caixa não encontrado!')
        }

        await cashierRepository.update(id, {
            startingAmount,
            endingAmount,
            startDate,
            endDate,
            countAmount
        })

        const cashierUpdate = await cashierRepository.findOne(id)

        return cashierUpdate
    }

}

