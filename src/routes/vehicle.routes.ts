import { Router } from 'express'
import VehicleController from '../controllers/VehicleController'

const vehicleRouter = Router()
const vehicleController = new VehicleController()


vehicleRouter.post("/", vehicleController.create)
 
vehicleRouter.get("/", vehicleController.list)

vehicleRouter.get("/:id", vehicleController.getById)

vehicleRouter.get("/get/license/:license", vehicleController.getBylicense)

vehicleRouter.delete("/:id", vehicleController.delete)

vehicleRouter.put("/:id", vehicleController.update)



export default vehicleRouter