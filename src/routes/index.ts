import { Router } from 'express'
import clientRouter from './client.routes'
import vehicleRouter from './vehicle.routes'
import employeeRouter from './employee.routes'
import userConfigRouter from './userConfig.routes'
import cashierRouter from './cashier.routes'
import amountSpotRouter from './amountSpot.routes'
import spotValueRouter from './spotValue.routes'
import parkingSpotRouter from './parkingSpot.routes'
import sparkingSpotHasVehicle from './sparkingSpotHasVehicle.routes'

const routes = Router()

routes.use('/client', clientRouter)
routes.use('/vehicle', vehicleRouter)
routes.use('/employee', employeeRouter)
routes.use('/userConfig', userConfigRouter)
routes.use('/cashier', cashierRouter)
routes.use('/amountSpot', amountSpotRouter)
routes.use('/spotValue', spotValueRouter)
routes.use('/parkingSpot', parkingSpotRouter)
routes.use('/parkingSpotHasVehicle', sparkingSpotHasVehicle)

export default routes