import { Router } from 'express'
import CashierController from '../controllers/CashierController'

const cashierRouter = Router()
const cashierController = new CashierController()


cashierRouter.post("/", cashierController.create)
 
cashierRouter.get("/", cashierController.list)

cashierRouter.get("/:id", cashierController.getById)

cashierRouter.delete("/:id", cashierController.delete)

cashierRouter.put("/:id", cashierController.update)


export default cashierRouter