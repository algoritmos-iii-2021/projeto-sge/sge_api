import { Router } from 'express'
import UserConfigController from '../controllers/UserConfigController'

const userConfigRouter = Router()
const userConfigController = new UserConfigController()


userConfigRouter.post("/", userConfigController.create)
 
userConfigRouter.get("/", userConfigController.list)

userConfigRouter.get("/:id", userConfigController.getById)

userConfigRouter.delete("/:id", userConfigController.delete)

userConfigRouter.put("/:id", userConfigController.update)


export default userConfigRouter