import { Router } from 'express'
import ParkingSpotHasVehicleController from '../controllers/ParkingSpotHasVehicleController'

const ParkingSpotHasVehicleRouter = Router()
const parkingSpotHasVehicleController = new ParkingSpotHasVehicleController()


ParkingSpotHasVehicleRouter.post("/", parkingSpotHasVehicleController.create)
 
ParkingSpotHasVehicleRouter.get("/", parkingSpotHasVehicleController.list)

ParkingSpotHasVehicleRouter.get("/:id", parkingSpotHasVehicleController.getById)

ParkingSpotHasVehicleRouter.delete("/:id", parkingSpotHasVehicleController.delete)

ParkingSpotHasVehicleRouter.put("/:id", parkingSpotHasVehicleController.update)

ParkingSpotHasVehicleRouter.put("/checkout/:id", parkingSpotHasVehicleController.leave)


export default ParkingSpotHasVehicleRouter;