export default function timeCalc(checkIn: Date, checkOut: Date, timeValue: number): number {
    
    var timeDiff = Math.abs(checkOut.getTime() - checkIn.getTime());
    var timehour = Math.abs(checkOut.getHours() - checkIn.getHours());
    var timemin = Number(((timeDiff / 60000) % 60).toFixed(0));

    if (checkIn.getMinutes() > checkOut.getMinutes()) {
        timehour--
    }

    var diffDays = Math.ceil((timeDiff / (1000 * 3600 * 24)) - 1);
    console.log('===========================================')
    console.log(diffDays + " Dias");
    console.log(timehour + ' Horas')
    console.log(timemin + ' Minutos')



    const calcHora = (timehour * timeValue)
    let calcMin
    let calcDay

    if (timemin <= 30) {
        calcMin = timeValue / 2
    } else {
        calcMin = timeValue
    }

    if (diffDays > 0) {
        calcDay = (diffDays * 24) * timeValue
    } else {
        calcDay = 0
    }

    const totalValue = calcHora + calcMin + calcDay

    return totalValue;
}