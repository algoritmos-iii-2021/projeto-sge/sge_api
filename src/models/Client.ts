import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity("client")
export default class Client {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string

    @Column()
    phone: string

    @Column()
    email: string

    @Column()
    address: string

    @Column()
    birth: Date

    @Column()
    cpf_cnpj: string

    @Column()
    pay_day: Date

    @Column()
    status: boolean

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}