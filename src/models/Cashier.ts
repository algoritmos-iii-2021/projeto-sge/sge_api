import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity("cashier")
export default class Cashier {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'starting_amount' })
    startingAmount: number

    @Column({ name: 'ending_amount', nullable: true })
    endingAmount: number

    @Column({ name: 'start_date' })
    startDate: Date

    @Column({ name: 'end_date' })
    endDate: Date

    @Column({ name: 'count_amount' })
    countAmount: number

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}