import { Column, CreateDateColumn, Entity, JoinColumn, OneToOne, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import Employee from "./Employee";

@Entity("userConfig")
export default class UserConfig {

    @PrimaryGeneratedColumn()
    id: number

    @OneToOne(() => Employee)
    @JoinColumn({ name: 'employee_id' })
    employeeConfigId: number

    @Column()
    report: boolean

    @Column()
    register: boolean

    @Column({ name: 'vehicle_update' })
    vehicleUpdate: boolean

    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}