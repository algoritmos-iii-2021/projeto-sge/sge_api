import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from "typeorm";

@Entity("spotValue")
export default class SpotValue {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    type: string;

    @Column()
    value: number;


    @CreateDateColumn({ name: 'Created_at' })
    createdAt: Date

    @UpdateDateColumn({ name: 'Updated_at' })
    updatedAt: Date
}