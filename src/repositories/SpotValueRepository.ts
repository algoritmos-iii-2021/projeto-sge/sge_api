import { EntityRepository, Repository } from "typeorm";
import SpotValue from "../models/SpotValue"

@EntityRepository(SpotValue) 
class SpotValueRepository extends Repository<SpotValue> {

}
export{SpotValueRepository}