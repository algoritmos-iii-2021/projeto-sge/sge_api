import { EntityRepository, Repository } from "typeorm";
import parkingSpot from "../models/ParkingSpot"

@EntityRepository(parkingSpot) 
class ParkingSpotRepository extends Repository<parkingSpot> {

}
export{ParkingSpotRepository}