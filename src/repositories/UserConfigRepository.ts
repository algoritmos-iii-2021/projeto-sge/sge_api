import { EntityRepository, Repository } from "typeorm";
import UserConfig from "../models/UserConfig"

@EntityRepository(UserConfig) 
class UserConfigRepository extends Repository<UserConfig> {

}
export{UserConfigRepository}