import { Request, Response } from "express"

import ParkingSpotService from '../services/ParkingSpotService'
const parkingSpotService = new ParkingSpotService()

export default class ParkingSpotController {
    async create(req: Request, res: Response) {
        try {
            const parkingSpot = await parkingSpotService.create(req.body)
            return res.status(201).json(parkingSpot)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await parkingSpotService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const parkingSpot = await parkingSpotService.getById(Number(req.params.id))
            return res.status(200).json(parkingSpot)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await parkingSpotService.delete(Number(req.params.id))
            return res.status(200).json('Vaga excluida com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const parkingSpotUpdate = await parkingSpotService.Update(Number(req.params.id), req.body)
            return res.status(200).json(parkingSpotUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
