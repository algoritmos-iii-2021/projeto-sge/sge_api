import { Request, Response } from "express"

import AmountSpotService from '../services/AmountSpotService'
const amountSpotService = new AmountSpotService()

export default class AmountSpotController {
    async create(req: Request, res: Response) {
        try {
            const amountSpot = await amountSpotService.create(req.body)
            return res.status(201).json(amountSpot)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await amountSpotService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }


    async update(req: Request, res: Response) {
        try {
            const amountSpotUpdate = await amountSpotService.Update(Number(req.params.id), req.body)
            return res.status(200).json(amountSpotUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
