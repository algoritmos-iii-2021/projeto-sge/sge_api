import { Request, Response } from "express"

import ParkingSpotHasVehicleService from '../services/ParkingSpotHasVehicleService'
const parkingSpotHasVehicleService = new ParkingSpotHasVehicleService()

export default class ParkingSpotHasVehicleController {
    async create(req: Request, res: Response) {
        try {
            const parkingSpotHasVehicle = await parkingSpotHasVehicleService.create(req.body)
            return res.status(201).json(parkingSpotHasVehicle)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await parkingSpotHasVehicleService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const parkingSpotHasVehicle = await parkingSpotHasVehicleService.getById(Number(req.params.id))
            return res.status(200).json(parkingSpotHasVehicle)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await parkingSpotHasVehicleService.delete(Number(req.params.id))
            return res.status(200).json('Veículo excluido com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleService.Update(Number(req.params.id), req.body)
            return res.status(200).json(parkingSpotHasVehicleUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
    
    async leave(req: Request, res: Response) {
        try {
            const parkingSpotHasVehicleUpdate = await parkingSpotHasVehicleService.leave(Number(req.params.id))
            return res.status(200).json(parkingSpotHasVehicleUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
