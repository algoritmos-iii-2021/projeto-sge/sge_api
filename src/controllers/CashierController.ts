import { Request, Response } from "express"

import CashierService from '../services/CashierService'
const cashierService = new CashierService()

export default class CashierController {
    async create(req: Request, res: Response) {
        try {
            const cashier = await cashierService.create(req.body)
            return res.status(201).json(cashier)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await cashierService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const cashier = await cashierService.getById(Number(req.params.id))
            return res.status(200).json(cashier)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await cashierService.delete(Number(req.params.id))
            return res.status(200).json('Caixa excluido com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const cashierUpdate = await cashierService.Update(Number(req.params.id), req.body)
            return res.status(200).json(cashierUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
