import { Request, Response } from "express"

import SpotValueService from '../services/SpotValueService'
const spotValueService = new SpotValueService()

export default class SpotValueController {
    async create(req: Request, res: Response) {
        try {
            const spotValue = await spotValueService.create(req.body)
            return res.status(201).json(spotValue)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await spotValueService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const spotValue = await spotValueService.getById(Number(req.params.id))
            return res.status(200).json(spotValue)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await spotValueService.delete(Number(req.params.id))
            return res.status(200).json('Vaga excluida com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const spotValueUpdate = await spotValueService.Update(Number(req.params.id), req.body)
            return res.status(200).json(spotValueUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
