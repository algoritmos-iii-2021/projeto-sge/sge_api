import { Request, Response } from "express"

import VehicleService from '../services/VehicleService'
const vehicleService = new VehicleService()
export default class VehicleController {
    async create(req: Request, res: Response) {
        try {
            const vehicle = await vehicleService.create(req.body)
            return res.status(201).json(vehicle)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async list(req: Request, res: Response) {
        try {
            const listAll = await vehicleService.list()
            return res.status(200).json(listAll)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getById(req: Request, res: Response) {
        try {
            const vehicle = await vehicleService.getById(Number(req.params.id))
            return res.status(200).json(vehicle)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async getBylicense(req: Request, res: Response){
        try {
            const vehicle = await vehicleService.getBylicense(req.params.license)
            return res.status(200).json(vehicle)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async delete(req: Request, res: Response) {
        try {
            await vehicleService.delete(Number(req.params.id))
            return res.status(200).json('Veículo excluido com sucesso!')
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }

    async update(req: Request, res: Response) {
        try {
            const vehicleUpdate = await vehicleService.Update(Number(req.params.id), req.body)
            return res.status(200).json(vehicleUpdate)
        } catch (error) {
            console.log(error)
            return res.status(error.statusCode).json(error.message)
        }
    }
}
